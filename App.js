/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import Router from './app/Router/router';
import { Provider } from 'react-redux'
import {createStore , applyMiddleware} from 'redux';
import reducers from './app/Reducers'
import ReduxThunk from 'redux-thunk';


class App extends React.Component {
 
  render() {

      const store= createStore(reducers, {} , applyMiddleware(ReduxThunk));

      return (
        <Provider store={store}>
          <Router/>
          
         </Provider>
        
      );
  }
}

export default App;
