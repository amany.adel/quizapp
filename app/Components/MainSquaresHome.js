import React from 'react';
import { View } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
    responsiveScreenHeight
} from "react-native-responsive-dimensions";
const MainSquaresHome = (props) => {
    return (
        <View style={[styles.containerStyle, props.style]}>

        {props.children}

        </View>
    );
};

const styles = {
    containerStyle: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: "space-between",
        borderTopWidth:1,
        borderColor:'#D5D8DB',
        paddingVertical:responsiveHeight(2)
    }    
};

export { MainSquaresHome };