import React from 'react';
import { View ,Image} from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
    responsiveScreenHeight
} from "react-native-responsive-dimensions";
const ImageHome = (props) => {
    return (
        <Image source={props.source} style={props.style}/>

    );
};


export { ImageHome };