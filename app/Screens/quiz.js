
import { View, Text, FlatList, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import React, { Component } from "react";
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Button } from 'react-native-elements';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
  responsiveScreenHeight
} from "react-native-responsive-dimensions";
console.disableYellowBox = true;

class Quiz extends Component {
  constructor(props) {
    super(props);
    this.temp = []
    this.randomArr = [];
    this.sumGradesValue=0;
    this.tmp = [];
    this.state = {
      first: true,
      second: false,
      third: false,
      forth: false,
      fifth: false,
      Quiz: [
        {
          ques: "What is 1004 divided by 2?",
          answers: ['52', '502', '520', '5002'],
          trueAnswer:
           '502',
          status: false

        },
        {
          ques: "106 × 106 – 94 × 94 = ?",
          answers: ['2004', '2400', '1902', '1906'],
          trueAnswer: '2400',
          status: false

        },
        {
          ques: "What is the greatest two digit number?",
          answers: ['10', '90', '11', '99'],
          trueAnswer: '99',
          status: false

        }, {
          ques: "How much is 90 – 19?",
          answers: ['71', '109', '89', ' None of these'],
          trueAnswer: '71',
          status: false

        }, {
          ques: "20 is divisible by ……… ",
          answers: ['1', '3', '7', ' None of these'],
          trueAnswer: '7',
          status: false
        }
      ],
      randomQuiz: [],
      grades:[]
    }

  }

  componentDidMount() {

    while (this.randomArr.length < 5) {

      this.temp.push(this.state.Quiz.splice(Math.floor(Math.random() * this.state.Quiz.length - 1), 1)[0]
      );
      this.randomArr.push(this.temp.pop());


    }
    console.log(this.randomArr);
    this.setState({ randomQuiz: this.randomArr });
    console.log("*********************************************************** test");
    console.log(this.state.randomQuiz)

  }

  render() {

    if (this.state.randomQuiz.length > 0) {
      return (
        <View>
          <View style={{
            marginTop:responsiveHeight(30)
          }}>

            {this.state.first ? (
              <View>
                <Text style={styles.quesText}> {this.state.randomQuiz[0].ques}</Text>
                <View>
                  <FlatList
                    data={this.state.randomQuiz[0].answers}
                    renderItem={({ item ,index}) => {
                      this.spin = Math.floor(Math.random()*this.state.randomQuiz[0].answers.length)

                      return (
                        <View style={{
                          marginTop:responsiveHeight(2)
                        }}>
                          <Button
                          titleStyle={{
                            color:'#FCB0B2',
                            fontSize:responsiveFontSize(2)
                          }}
                            title={item}
                            type="clear"
                            onPress={() => {
                              {item==this.state.randomQuiz[0].trueAnswer?(

                              this.setState({
                                first: false,
                                second: true
                              },()=>{
                                this.sumGradesValue +=2;
                              }))
                              :
                              this.setState({
                                first: false,
                                second: true
                              })}
                            }}
                          />
                        </View>);
                    }}
                    keyExtractor={({item,index}) => index}

                  // numColumns={3}
                  />
                </View>
              </View>
            ) : null}

            {this.state.second ? (
              <View>
                <Text style={styles.quesText}> {this.state.randomQuiz[1].ques}</Text>

                <FlatList
                  data={this.state.randomQuiz[1].answers}
                  renderItem={({ item ,index}) => {
                    return (
                      <View style={{
                        marginTop:responsiveHeight(2)
                      }}>
                        <Button
                         titleStyle={{
                          color:'#FCB0B2',
                          fontSize:responsiveFontSize(2)
                        }}
                          title={item
                            // this.state.randomQuiz[1].answers[Math.floor(Math.random() * 3)]
                          }
                          type="clear"
                          onPress={() => {
                            {item==this.state.randomQuiz[1].trueAnswer?
                              (
                            this.setState({
                              second: false,
                              third: true,
                            },()=>{
                                this.sumGradesValue +=2;
                              })):(
                              this.setState({
                                second: false,
                                third: true,

                              })
                            )}
                          }}
                        />
                      </View>);
                  }}
                  keyExtractor={({item,index}) => index}

                // numColumns={3}
                />




              </View>
            ) : null}

            {this.state.third ? (
              <View>
                <Text style={styles.quesText}> {this.state.randomQuiz[2].ques}</Text>

                <FlatList
                    data={this.state.randomQuiz[2].answers}
                    renderItem={({ item,index }) => {
                      return (
                        <View style={{
                          marginTop:responsiveHeight(2)
                        }}>
                          <Button
                           titleStyle={{
                            color:'#FCB0B2',
                            fontSize:responsiveFontSize(2)
                          }}
                            title={item
                              // this.state.randomQuiz[2].answers[Math.floor(Math.random() * 3)]
                            }
                            type="clear"
                            onPress={() => {
                              {item==this.state.randomQuiz[2].trueAnswer?

                              (this.setState({
                                third: false,
                                forth: true,
                              },()=>{
                                this.sumGradesValue +=2;
                              })):(
                                this.setState({
                                  third: false,
                                  forth: true,

                                })
                              )}
                            }}
                          />
                        </View>);
                    }}
                    keyExtractor={({item,index}) => index}

                  // numColumns={3}
                  />
            

              </View>
            ) : null}

            {this.state.forth ? (
              <View>
                <Text style={styles.quesText}> {this.state.randomQuiz[3].ques}</Text>

         
              <FlatList
                    data={this.state.randomQuiz[3].answers}
                    renderItem={({ item,index }) => {
                      return (
                        <View style={{
                          marginTop:responsiveHeight(2)
                        }}>
                          <Button
                           titleStyle={{
                            color:'#FCB0B2',
                            fontSize:responsiveFontSize(2)
                          }}
                            title={item
                              // this.state.randomQuiz[3].answers[Math.floor(Math.random() * 3)]
                            }
                            type="clear"
                            onPress={() => {
                              {item==this.state.randomQuiz[3].trueAnswer?

                              (this.setState({
                                forth: false,
                                fifth:true
                              },()=>{
                                this.sumGradesValue +=2;
                              })):(
                                this.setState({
                                  forth: false,
                                  fifth:true,

                                })
                              )}
                            }}
                          />
                        </View>);
                    }}
                    keyExtractor={({item,index}) => index}

                  // numColumns={3}
                  />



              </View>
            ) : null}


            {this.state.fifth ? (
              <View>
                <Text style={styles.quesText}> {this.state.randomQuiz[4].ques}</Text>

             <FlatList
                    data={this.state.randomQuiz[4].answers}
                    renderItem={({ item,index }) => {
                      return (
                        <View style={{
                          marginTop:responsiveHeight(2)
                        }}>
                          <Button
                           titleStyle={{
                            color:'#FCB0B2',
                            fontSize:responsiveFontSize(2)
                          }}
                            title={item
                              // this.state.randomQuiz[4].answers[Math.floor(Math.random() * 3)]
                            }
                            type="clear"
                            onPress={() => {
                              {item==this.state.randomQuiz[4].trueAnswer?

                              (this.setState({
                                fifth: false,

                              },()=>{
                                this.sumGradesValue +=2;
                              })):(
                                this.setState({
                                  fifth: false,

                                })
                              )}
                            }}
                          />
                        </View>);
                    }}
                    keyExtractor={({item,index}) => index}

                  // numColumns={3}
                  />
             



              </View>
            ) : null}

{!this.state.first &&
!this.state.second &&
!this.state.third &&
!this.state.forth &&
!this.state.fifth?(
  <View >
  <Text style={styles.resText}>
    Hello {this.props.username} ,
  </Text>
  <Text style={styles.resText}>
    Your grades are 
  </Text>
  <Text style={styles.resText}>
    {this.sumGradesValue} 
  </Text>
  <Text style={{
    textAlign:'center'
  }}>
  <Icon name="dice" size={80} color="#FAB0AD" />
  </Text>
  
</View>
):
null}
           

          </View>

          <View>
          </View>
        </View>
      );
    }

    else {
      return (
        <View>
          <Text> Wait for Loading Data </Text>
        </View>
      );
    }

  }
}

const styles = StyleSheet.create({
  quesText:{
    fontSize: responsiveFontSize(3.3),
    color: '#76D0D1',
    textAlign:'center'
  },
  resText:{
    fontSize:responsiveFontSize(5),
    textAlign:'center',
    color:'#89CDCE',
    margin:responsiveScreenHeight(2)

  }
});


const mapStateToProps = state => {
  return {
      username: state.auth.username,



  };

};

const QuizRedux = connect(mapStateToProps, {


})(Quiz);
export { QuizRedux as Quiz };
