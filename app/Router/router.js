import React from 'react';
import {
  Login,
  Quiz,
  Result,
} from '../Screens';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const RouterComponent = () => {

  return (

    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login"
       screenOptions={{
        headerShown: false
      }}
      >

        <Stack.Screen
          name="Quiz"
          component={Quiz}
        /> 
        <Stack.Screen
          name="Login"
          component={Login}

        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default RouterComponent;